* [[SDK Tools]] : how to rebuild the latest SDK Tools
* [[SDK]] : how to rebuild the different versions of the SDK
* [[NDK 10]] : how to rebuild NDK r10
* [[Prebuilts]] : how to rebuild the precompiled tools stored in `prebuilts`
* [[Terms and Conditions]] : various versions of the SDK click-wrapping "Terms and Conditions", aka non-free EULA

----

All wikis are supposed to have a [[SandBox]], so this one does too.

This wiki is powered by [[ikiwiki]].
