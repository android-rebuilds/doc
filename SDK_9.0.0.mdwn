**SDK 9.0.0 (GNU/Linux, Windows)**

[[!toc]]

## Recreate matching build environment

The current reference build environment for this version is [[Ubuntu 14.04|Build_environments]].

## Build dependencies

    # https://source.android.com/setup/build/initializing
    dpkg --add-architecture i386
    apt-get update

    apt-get -y install wget
    wget http://old-releases.ubuntu.com/ubuntu/pool/universe/o/openjdk-8/openjdk-8-jre-headless_8u45-b14-1_amd64.deb
    wget http://old-releases.ubuntu.com/ubuntu/pool/universe/o/openjdk-8/openjdk-8-jre_8u45-b14-1_amd64.deb
    wget http://old-releases.ubuntu.com/ubuntu/pool/universe/o/openjdk-8/openjdk-8-jdk_8u45-b14-1_amd64.deb
    checksumfile=$(mktemp)
    cat <<'EOF' > $checksumfile
    0f5aba8db39088283b51e00054813063173a4d8809f70033976f83e214ab56c0  openjdk-8-jre-headless_8u45-b14-1_amd64.deb
    SHA256 9ef76c4562d39432b69baf6c18f199707c5c56a5b4566847df908b7d74e15849  openjdk-8-jre_8u45-b14-1_amd64.deb
    SHA256 6e47215cf6205aa829e6a0a64985075bd29d1f428a4006a80c9db371c2fc3c4c  openjdk-8-jdk_8u45-b14-1_amd64.deb
    EOF
    sha256sum -c $checksumfile || exit 1
    dpkg -i openjdk-8-jdk_8u45-b14-1_amd64.deb \
      openjdk-8-jre-headless_8u45-b14-1_amd64.deb \
      openjdk-8-jre_8u45-b14-1_amd64.deb || true
    apt-get -y -f install

    apt-get -y install git-core gnupg flex bison gperf build-essential zip \
      curl zlib1g-dev gcc-multilib g++-multilib libc6-dev-i386 \
      lib32ncurses5-dev x11proto-core-dev libx11-dev lib32z-dev \
      libgl1-mesa-dev libxml2-utils xsltproc unzip

    # TODO: are hose still necessary?
    apt-get -y install python-networkx
    apt-get -y install zlib1g-dev:i386

    # Windows dependencies
    # https://sites.google.com/a/android.com/tools/build  2015-07-23
    apt-get -y install mingw32 tofrodos


## Prepare source tree

Install `repo`:

    sudo apt-get install curl ca-certificates git python
    mkdir ~/bin/
    curl https://storage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
    chmod a+x ~/bin/repo

    # avoid prompts
    git config --global user.email "you@example.com"
    git config --global user.name "Your Name"
    git config --global color.ui true

Checkout the source:

    mkdir ~/wd/
    cd ~/wd/
    ~/bin/repo init -u https://android.googlesource.com/platform/manifest -b android-9.0.0_r21
    time ~/bin/repo sync --current-branch -j4
    # 1h15 with fiber connection
    # 32GB .repo

## Build the SDK

    # Optional: reserve 10GB for ccache if you're investigating how to build or patching something
    # Cf. https://source.android.com/source/initializing.html#ccache
    #export USE_CCACHE=1
    #prebuilts/misc/linux-x86/ccache/ccache -M 10G

    export BUILD_NUMBER="user.9.0.0_r21"
    . build/envsetup.sh
    lunch sdk-user
    time make sdk -j$(nproc) showcommands dist
    
    time make win_sdk -j$(nproc) showcommands dist
    
Results:

    -rw-r--r-- 1 android android 729M Dec  7 23:30 out/dist/android-sdk_user.9.0.0_r21_linux-x86.zip
    -rw-r--r-- 1 android android 731M Dec  7 23:32 out/dist/android-sdk_user.9.0.0_r21_windows.zip
    -rw-r--r-- 1 android android 729M Dec  7 23:30 out/host/linux-x86/sdk/sdk/android-sdk_user.9.0.0_r21_linux-x86.zip
    -rw-r--r-- 1 android android 731M Dec  7 23:32 out/host/windows/sdk/sdk/android-sdk_user.9.0.0_r21_windows.zip


## TODO

Rebuild the `prebuilts/` sub-directories.


## Error messages and resolutions

Prepare a LOT of disk space.


## Notes

### Build result recap

r45

    $ JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64 tools/bin/sdkmanager --list
    Installed packages:
      Path                                         | Version    | Description                       | Location                            
      -------                                      | -------    | -------                           | -------                             
      build-tools;28.0.3                           | 28.0.3     | Android SDK Build-Tools 28.0.3    | build-tools/android-9/              
      docs                                         | 1          | Documentation for Android SDK     | docs/                               
      platform-tools                               | 28.0.0 rc1 | Android SDK Platform-Tools 28 rc1 | platform-tools/                     
      platforms;android-28                         | 6          | Android SDK Platform 28, rev 6    | platforms/android-9/                
      system-images;android-28;default;armeabi-v7a | 4          | ARM EABI v7a System Image         | system-images/android-9/armeabi-v7a/
    
    Available Updates:
      ID             | Installed  | Available
      -------        | -------    | -------  
      platform-tools | 28.0.0 rc1 | 29.0.1   

r33

    $ JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64 tools/bin/sdkmanager --list
    Installed packages:
      Path                                         | Version    | Description                       | Location                            
      -------                                      | -------    | -------                           | -------                             
      build-tools;28.0.3                           | 28.0.3     | Android SDK Build-Tools 28.0.3    | build-tools/android-9/              
      docs                                         | 1          | Documentation for Android SDK     | docs/                               
      platform-tools                               | 28.0.0 rc1 | Android SDK Platform-Tools 28 rc1 | platform-tools/                     
      platforms;android-28                         | 6          | Android SDK Platform 28, rev 6    | platforms/android-9/                
      system-images;android-28;default;armeabi-v7a | 4          | ARM EABI v7a System Image         | system-images/android-9/armeabi-v7a/
      system-images;android-28;default;x86_64      | 4          | Intel x86 Atom_64 System Image    | system-images/android-9/x86_64/     
    
    Available Updates:
      ID             | Installed  | Available
      -------        | -------    | -------  
      platform-tools | 28.0.0 rc1 | 28.0.1   

r30

    $ JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64 tools/bin/sdkmanager --list
    Installed packages:

      Path                                         | Version    | Description                       | Location                            
      -------                                      | -------    | -------                           | -------                             
      build-tools;28.0.2                           | 28.0.2     | Android SDK Build-Tools 28.0.2    | build-tools/android-9/              
      docs                                         | 1          | Documentation for Android SDK     | docs/                               
      platform-tools                               | 28.0.0 rc1 | Android SDK Platform-Tools 28 rc1 | platform-tools/                     
      platforms;android-28                         | 6          | Android SDK Platform 28, rev 6    | platforms/android-9/                
      system-images;android-28;default;armeabi-v7a | 4          | ARM EABI v7a System Image         | system-images/android-9/armeabi-v7a/


r21

    $ JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64 tools/bin/sdkmanager --list
    Installed packages:
      Path                                         | Version    | Description                       | Location                            
      -------                                      | -------    | -------                           | -------                             
      build-tools;28.0.2                           | 28.0.2     | Android SDK Build-Tools 28.0.2    | build-tools/android-9/              
      docs                                         | 1          | Documentation for Android SDK     | docs/                               
      platform-tools                               | 28.0.0 rc1 | Android SDK Platform-Tools 28 rc1 | platform-tools/                     
      platforms;android-28                         | 6          | Android SDK Platform 28, rev 6    | platforms/android-9/                
      system-images;android-28;default;armeabi-v7a | 4          | ARM EABI v7a System Image         | system-images/android-9/armeabi-v7a/
    
    Available Updates:
      ID             | Installed  | Available
      -------        | -------    | -------  
      platform-tools | 28.0.0 rc1 | 28.0.1   

r18

    $ tools/bin/sdkmanager --list
    Installed packages:
      Path                                         | Version    | Description                       | Location                            
      -------                                      | -------    | -------                           | -------                             
      build-tools;28.0.1                           | 28.0.1     | Android SDK Build-Tools 28.0.1    | build-tools/android-9/              
      docs                                         | 1          | Documentation for Android SDK     | docs/                               
      platform-tools                               | 28.0.0 rc1 | Android SDK Platform-Tools 28 rc1 | platform-tools/                     
      platforms;android-28                         | 5          | Android SDK Platform 28, rev 5    | platforms/android-9/                
      system-images;android-28;default;armeabi-v7a | 3          | ARM EABI v7a System Image         | system-images/android-9/armeabi-v7a/

Using [[SDK_Tools]]:

      tools                                        | 26.1.1     | Android SDK Tools 26.1.1          | tools/                         


### ccache usage

    N/A


### build stats

r45, using i5-6300HQ (4x2.30GHz), SSD disk, with `dist` and `sdk_repo` targets:

* win_sdk: build completed successfully (04:30:24 (hh:mm:ss))
* du -sh wd/: 199G
* du -sh wd/.repo/: 37G

r33, using i5-6300HQ (4x2.30GHz), SSD disk, with `dist` and `sdk_repo` targets:

* sdk: build completed successfully (03:53:13 (hh:mm:ss))
* win_sdk: build completed successfully (02:43:33 (hh:mm:ss))
* PRODUCT-sdk_x86_64-sdk: build completed successfully (03:53:15 (hh:mm:ss))
* du -sh wd/: 319G (additional system image: +93GB)
* du -sh wd/.repo/: 36G

r21, using i5-6300HQ (4x2.30GHz), SSD disk:

* make sdk: build completed successfully (03:17:40 (hh:mm:ss))
* make win_sdk: build completed successfully (02:15:22 (hh:mm:ss))
* du -sh wd/: 205G
* du -sh wd/.repo/: 33G


## Automated build recipe

[[https://gitlab.com/android-rebuilds/auto/tree/master/sdk-9.0.0]]
