**SDK 1.6 (GNU/Linux)**

[[!toc]]

## Recreate matching build environment

The current reference build environment for this version is [[Ubuntu 6.06|Build_environments]].

## Build dependencies

    apt-get update
    
    wget --quiet http://old-releases.ubuntu.com/ubuntu/pool/multiverse/s/sun-java5/sun-java5-jdk_1.5.0-22-0ubuntu0.6.06.1_i386.deb
    wget --quiet http://old-releases.ubuntu.com/ubuntu/pool/multiverse/s/sun-java5/sun-java5-jre_1.5.0-22-0ubuntu0.6.06.1_all.deb
    wget --quiet http://old-releases.ubuntu.com/ubuntu/pool/multiverse/s/sun-java5/sun-java5-bin_1.5.0-22-0ubuntu0.6.06.1_i386.deb
    wget --quiet http://old-releases.ubuntu.com/ubuntu/pool/multiverse/s/sun-java5/sun-java5-demo_1.5.0-22-0ubuntu0.6.06.1_i386.deb
    checksumfile=$(mktemp)
    cat <<'EOF' > $checksumfile
    1535def92cc90f53f07261ec85417599c34f1c0f  sun-java5-bin_1.5.0-22-0ubuntu0.6.06.1_i386.deb
    3bf97ce887dcfeb8393b8d798ef26dc80e0959da  sun-java5-demo_1.5.0-22-0ubuntu0.6.06.1_i386.deb
    2084de7b5f4a46d898d1f8650b117754145e287e  sun-java5-jdk_1.5.0-22-0ubuntu0.6.06.1_i386.deb
    25e3df7ae51fbb915344fb55e5f5716e7df6155f  sun-java5-jre_1.5.0-22-0ubuntu0.6.06.1_all.deb
    EOF
    sha1sum -c $checksumfile || exit 1
    
    dpkg -i \
      sun-java5-jdk_1.5.0-22-0ubuntu0.6.06.1_i386.deb \
      sun-java5-jre_1.5.0-22-0ubuntu0.6.06.1_all.deb \
      sun-java5-bin_1.5.0-22-0ubuntu0.6.06.1_i386.deb \
      sun-java5-demo_1.5.0-22-0ubuntu0.6.06.1_i386.deb \
      || true
    apt-get -y -f install
    
    apt-get install -y git-core gnupg flex bison gperf build-essential zip \
      curl zlib1g-dev libc6-dev libncurses5-dev \
      x11proto-core-dev libx11-dev libreadline5-dev libz-dev \
      libgl1-mesa-dev g++-multilib mingw32 tofrodos python-markdown \
      libxml2-utils xsltproc unzip

## Prepare source tree

Install [[repo]].

Checkout the source in a recent (Python 3) separate environment:

Checkout the source:

    mkdir ~/wd/
    cd ~/wd/
    ~/bin/repo init -u https://android.googlesource.com/platform/manifest -b android-1.6_r2 --depth=1
    time ~/bin/repo sync --current-branch -j4
    # 0.5GB .repo

## Build the SDK

    export BUILD_NUMBER="userdebug.1.6_r2"
    . build/envsetup.sh
    lunch sdk-userdebug
    time make sdk -jXX showcommands dist

Results:

    -rw-r--r-- 1 android android 111M Jul  9 11:20 out/dist/android-sdk_userdebug.1.6_r2_linux-x86.zip
    -rw-r--r-- 1 android android 111M Jul  9 11:20 out/host/linux-x86/sdk/android-sdk_userdebug.1.6_r2_linux-x86.zip
    -rw-r--r-- 1 android android 111M Mar 16 19:17 out/dist/android-sdk_eng.android_linux-x86.zip
    -rw-r--r-- 1 android android 111M Mar 16 19:17 out/host/linux-x86/sdk/android-sdk_eng.android_linux-x86.zip


## TODO

Rebuild the `prebuilts/` sub-directories.


## Error messages and resolutions

    /usr/bin/ld: skipping incompatible /usr/lib/libX11.a when searching for -lX11
    /usr/bin/ld: cannot find -lX11

Requires Ubuntu 32-bit. Possible work-around: <https://web.archive.org/web/20100722110817/http://source.android.com:80/source/download.html>

    host layoutlib_create: out/host/common/obj/JAVA_LIBRARIES/temp_layoutlib_intermediates/javalib.jar
    java -jar out/host/common/obj/JAVA_LIBRARIES/layoutlib_create_intermediates/javalib.jar \
    	             out/host/common/obj/JAVA_LIBRARIES/temp_layoutlib_intermediates/javalib.jar \
    	             out/target/common/obj/JAVA_LIBRARIES/core_intermediates/classes.jar \
    	             out/target/common/obj/JAVA_LIBRARIES/framework_intermediates/classes.jar
    Output: out/host/common/obj/JAVA_LIBRARIES/temp_layoutlib_intermediates/javalib.jar
    Input :      out/target/common/obj/JAVA_LIBRARIES/core_intermediates/classes.jar
    Input :      out/target/common/obj/JAVA_LIBRARIES/framework_intermediates/classes.jar
    Exception in thread "main" java.lang.NoClassDefFoundError: org/objectweb/asm/ClassVisitor
    	at com.android.tools.layoutlib.create.Main.main(Main.java:45)
    make: *** [out/host/common/obj/JAVA_LIBRARIES/temp_layoutlib_intermediates/javalib.jar] Error 1
    make: *** Waiting for unfinished jobs....
=> `apt-get install unzip` + restart the build from scratch

Missing `unzip` causes non-fatal errors where the build fails to unzip existing `.jar` dependencies for re-bundling in new `jar`s.


## Notes

### Build result recap

    $ JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64 tools/bin/sdkmanager --list
    Installed packages:
      Path                | Version | Description                   | Location              
      -------             | ------- | -------                       | -------               
      docs                | 1       | Documentation for Android SDK | docs/                 
      platforms;android-4 | 2       | Android SDK Platform 4, rev 2 | platforms/android-1.6/
      tools               | 2.0.0   | Android SDK Tools 2           | tools/                

### build stats

r2, using i5-6300HQ (4x2.30GHz), SSD disk, with `dist` target:

* sdk: 13mn
* du -sh wd/: 6.1G
* du -sh wd/.repo/: 0.5G

## Automated build recipe

[[https://gitlab.com/android-rebuilds/auto/tree/master/sdk-1.6]]
