# Build instructions

* [[SDK 1.6]]
* [[SDK 2.1]]
* [[SDK 2.2]]
* [[SDK 5.1.1]]
* [[SDK 6.0.0]]
* [[SDK 6.0.1]]
* [[SDK 8.0.0]]
* [[SDK 8.1.0]]
* [[SDK 9.0.0]]
* [[SDK 10.0.0]]
* [[SDK 11.0.0]]


# General considerations

The SDK is built by specifying:

- an [Android version](https://android.googlesource.com/platform/manifest/+refs) (8.1.0_r53, 9.0.0_r21...)
- a [build variant](https://source.android.com/setup/build/building#choose-a-target) (eng, user, userdebug)
- a [build goal/target](https://android.googlesource.com/platform/sdk/+/master/docs/howto_build_SDK.txt) (sdk, win_sdk, win-sdktools...)
- a [build product/architecture](https://android.googlesource.com/platform/build/+/master/target/product/) (sdk=sdk_phone_armv7, sdk_arm64...)
- build modifier goals/targets (showcommands, dist, sdk_repo...)

Not sure what build variant should be used: `user` should be optimized (but fails sometimes for `win_sdk` targets), documentation mentions to `eng`, and system images product targets default to `userdebug` (cf. `build.prop` in the zip archive).

Product and variant can be selected using [lunch](https://android.googlesource.com/platform/build/+/master/envsetup.sh):

    lunch <product_name>-<build_variant>
    e.g. lunch sdk-eng

or using a [PRODUCT target](https://android.googlesource.com/platform/build/+/6f8e3da0356abea6466ac08796022cc9bf698ffc/core/product_config.mk):

    PRODUCT-$TARGET_PRODUCT-$TARGET_BUILD_VARIANT
      if $TARGET_BUILD_VARIANT is in (user, userdebug, eng)
    
    PRODUCT-$TARGET_PRODUCT-$GOAL
      => TARGET_PRODUCT=$TARGET_PRODUCT make $GOAL
         variant 'userdebug'
    e.g. make PRODUCT-sdk_arm64-sdk => TARGET_PRODUCT='sdk_arm64' make sdk
    
    Note: PRODUCT-sdk_x86_64-userdebug gives "error: The 'sdk' target may not be specified with any other targets"

with SDK 11 PRODUCT-* is no longer supported, you define variables directly:

    lunch sdk-userdebug && m TARGET_PRODUCT=sdk_x86_64  sdk  dist sdk_repo
    #m TARGET_PRODUCT=sdk_x86_64 TARGET_BUILD_VARIANT=userdebug  sdk  dist sdk_repo  # doesn't set build variant everywhere

The build architecture allow building emulator system images for other architectures than armv7 (x86, arm64...).

Modifiers:

- `showcommands`: verbose mode (up to v10, replaced by `out/dist/logs/verbose.log.gz`)
- `dist`: in addition to scattering files in `out/host/`, copies important build results in `out/dist/`
- `sdk_repo`: re-zips the SDK as individual components (build-tools, platform...) and generates XML for a sdkmanager repository


# Targeted rebuilds

- System image for emulators + F-Droid Privileges Extension: <https://gitlab.com/fdroid/emulator-system-images> + <https://forum.f-droid.org/t/call-for-help-making-free-software-builds-of-the-android-sdk/4685/40>
- Rebuild the source packages: <https://gist.github.com/cketti/71475220a08c9ee43fd48ecaaf7046a5>

# Package versions

Unrelated to binaries versions.  
Check the [development.git](https://android.googlesource.com/platform/development/+/refs/heads/master/sdk/) repository to locate version changes.
