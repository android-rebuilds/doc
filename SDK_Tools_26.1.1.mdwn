**SDK Tools 26.1.1 (GNU/Linux, Windows)**

[[!toc ]]

## Principle of operation

The Android SDK contains the *SDK Tools*, which are built from a different source tree than the [[SDK]].


## Recreate matching build environment

The current reference build environment for this version is [[Ubuntu 14.04|Build_environments]].


## Build dependencies

    # OpenJDK 8 N/A
    #apt-get -y install openjdk-8-jdk
    apt-get -y install wget
    wget http://old-releases.ubuntu.com/ubuntu/pool/universe/o/openjdk-8/openjdk-8-jre-headless_8u45-b14-1_amd64.deb
    wget http://old-releases.ubuntu.com/ubuntu/pool/universe/o/openjdk-8/openjdk-8-jre_8u45-b14-1_amd64.deb
    wget http://old-releases.ubuntu.com/ubuntu/pool/universe/o/openjdk-8/openjdk-8-jdk_8u45-b14-1_amd64.deb
    checksumfile=$(mktemp)
    cat <<'EOF' > $checksumfile
    0f5aba8db39088283b51e00054813063173a4d8809f70033976f83e214ab56c0  openjdk-8-jre-headless_8u45-b14-1_amd64.deb
    SHA256 9ef76c4562d39432b69baf6c18f199707c5c56a5b4566847df908b7d74e15849  openjdk-8-jre_8u45-b14-1_amd64.deb
    SHA256 6e47215cf6205aa829e6a0a64985075bd29d1f428a4006a80c9db371c2fc3c4c  openjdk-8-jdk_8u45-b14-1_amd64.deb
    EOF
    sha256sum $checksumfile || exit "Incorrect checksums"
    dpkg -i openjdk-8-jdk_8u45-b14-1_amd64.deb \
      openjdk-8-jre-headless_8u45-b14-1_amd64.deb \
      openjdk-8-jre_8u45-b14-1_amd64.deb || true
    apt-get -y -f install
    
    
    # Compiles 32-bit C++ programs
    # 'emugen' compiled and run as 32-bit
    apt-get -y install make binutils patch
    apt-get -y install g++-multilib
    apt-get -y install zip unzip libxml2-utils  # zip/unzip/zip + xmllint


## Locating the source

According to the <https://android.googlesource.com/platform/tools/base/+/studio-master-dev/source.md>, `studio-3.2.1` is the current stable version.


## Preparing the source tree

Install `repo`:

    sudo apt-get install curl ca-certificates git python
    mkdir ~/bin/
    curl https://storage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
    chmod a+x ~/bin/repo

    # avoid prompts
    git config --global user.email "you@example.com"
    git config --global user.name "Your Name"
    git config --global color.ui true

Shallow checkout of the source:

    ~/bin/repo init -u https://android.googlesource.com/platform/manifest -b studio-3.2.1
    # version == 26.1.1 according to 'tools/base/files/tools_source.properties'
    ~/bin/repo sync -c -j4 -q
    # 30mn with good connectivity
    # ??GB (inc. 13GB .repo)

## Building SDK Tools

    mkdir -p out/dist
    # Fix silly mistake
    sed -i -e 's/${GRADLEW} $1/${GRADLEW} $*/' tools/buildSrc/servers/build_tools.sh
    time bash -x tools/buildSrc/servers/build_tools.sh `pwd`/out `pwd`/out/dist 0  # doesn't accept 26.1.1 as last argument :/
    # Note: if needed for speed-up: remove target 'makeWinSdk' in that script
    
    # Android Studio
    # https://android.googlesource.com/platform/tools/base/+/studio-master-dev/studio.md
    #tools/base/bazel/bazel version
    (
        cd tools/idea/
        bash -x ./build_studio.sh
    )
    
    # Gradle plugin
    # https://android.googlesource.com/platform/tools/base/+/studio-master-dev/build-system/README.md
    # http://tools.android.com/build/gradleplugin
    (
        cd tools/
        ./gradlew assemble
        # TODO: this asks for a SDK, plus with more recent buildtools than we just compiled
        ./gradlew publishLocal || true
    )
    
    # 30mn, 42GB (inc. 13GB .repo)

This results in:

    out/dist/  # SDK tools
    tools/idea/out/studio/dist/  # Android Studio
    out/repo/  # Android Gradle plugin

We just compiled Tools > Android SDK Tools 26.1.1!


## TODO

### prebuilts

We should be able to compile without (untrusted) prebuilts

### Android Gradle Plugin

We're supposed to use the Android Gradle Plugin like this in your `build.gradle`:

    buildscript {
       repositories {
           jcenter()
           maven { url '.../out/repo' }
       }
       dependencies {
           classpath 'com.android.tools.build:gradle:3.2.0-dev'

but the resulting repo lacks packages `com.android.tools.build:bundletool:0.5.0 com.android.tools.build.jetifier:jetifier-core:1.0.0-alpha10 com.android.tools.build.jetifier:jetifier-processor:1.0.0-alpha10`
which are present in the prebuilts.

### Emulator

The emulator component is now built separately, see [[Emulator]].

## Automated build recipe

[[https://gitlab.com/android-rebuilds/auto/tree/master/sdk_tools-26.1.1]]
